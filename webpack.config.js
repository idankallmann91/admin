require('dotenv').config()

const path = require('path'),
      { MODE } = process.env

module.exports = {

  mode: MODE !== 'production' ? 'development' : 'production',
  entry: path.join(__dirname, 'public', 'src', 'main.js'),

  output: {
    path: path.join(__dirname, 'public', 'dist'),
    filename: 'bundle.js'
  },

  // devtool: 'cheap-eval-source-map',

  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env', '@babel/preset-react']
          }
        }
       },
      { test: /\.css$/, use: ['style-loader', 'css-loader'] }
    ]
  },


  watch: MODE !== 'production' ? true : false,
  watchOptions: {
    ignored: /node_modules/,
    aggregateTimeout: 600
  }

}
