require('dotenv').config()

const express = require('express'),
      session = require('client-sessions'),
      hbs = require('express-handlebars'),
      bodyParser = require('body-parser'),
      helmet = require('helmet'),
      morgan = require('morgan'),
      path = require('path'),
      { PORT, MODE, SECRET_KEY } = process.env,
      app = express()

// View engine
app.engine('hbs', hbs({
  extname: 'hbs'
}))
app.set('view engine', 'hbs')

// Middlewares
// app.use(favicon(
//   path.join(__dirname, '/public/images/favicon/favicon.jpg')
// ))
app.use(helmet())
app.use(morgan('dev'))
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(session({
  cookieName: 'session',
  secret: SECRET_KEY,
  duration: 60 * 60 * 1000,
  activeDuration: 5 * 60 * 1000
}))
app.use(express.static(path.join(__dirname, 'public', 'dist')))

// Routing (mainR route should be placed last)
app.use('/', require('./routes/users-r'))
app.use('/api', require('./routes/board-r'))
app.use('/', require('./routes/main-r'))

app.listen(PORT, () => {
  console.log(`App running port ${PORT}`)
})
