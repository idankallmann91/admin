const router = require('express').Router(),
      path = require('path'),
      multer = require('multer'),

      storage = multer.diskStorage({
        destination: (req, file, cb) => {
          if (path.extname(file.originalname).toLowerCase() === '.mp3') {
            cb(null, path.join(__dirname, '..', 'uploads', 'audios'))
          } else {
            cb(null, path.join(__dirname, '..', 'uploads', 'images'))
          }
        },
        filename: (req, file, cb) => {
          let fileName = `${Date.now()}-${file.originalname}`
          cb(null, fileName)
        }
      }),
      upload = multer({
        storage: storage,
        limits: { fileSize: 25000000 },
        fileFilter: (req, file, cb) => checkFileType(file,cb)
       }).any()


router.get('/toggle-button', (req, res) => {
  let { btn } = req.query,
      keyboard = btn === 'false' ? true : false

  res.json({success: true, keyboard})
})

router.post('/upload-files' ,(req, res) => {
  upload(req, res, err => {
    if (err instanceof multer.MulterError || err) {
      return res.json({success: false, mssg: 'Error while try uploading'})
    }
    res.json({success: true, mssg: 'Success uploading'})
  })
})

router.get('/text-field' ,(req, res) => {
  let { text } = req.query

  if(!text) return res.json({success: false, mssg: 'Must provide text'})

  console.log(text);

  res.json({success: true, data: text, mssg: 'Success save text'})
})

const checkFileType = (file, cb) => {
  const fileType = /jpeg|jpg|png|gif|mp3/,
        orginialName = path.extname(file.originalname).toLowerCase()
        extname = fileType.test(orginialName),
        mimetype = fileType.test(file.mimetype)

  return (mimetype && extname) ? cb(null, true) : cb('Error')
}

module.exports = router
