const router = require('express').Router(),
      path = require('path'),
      fs = require('fs'),
      mw = require('../config/middlewares.js')

router.get('/404', mw.NotLoggedIn, (req, res) => {
  res.render('404')
})

router.get('*', mw.LoggedIn, (req, res) => {
  res.render('app')
})

module.exports = router
