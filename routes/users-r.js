const router = require('express').Router(),
      mw = require('../config/middlewares.js'),
      {
        findUserByUsername,
        comparePassword,
        cryptPassword,
        createUser,
        deleteUser
      } = require('../config/db.js')

router.get('/login', mw.NotLoggedIn, (req, res) => res.render('login'))

router.get('/logout', mw.LoggedIn, (req, res) => {
  let url = req.session.reset() ? '/login' : '/'
  res.redirect(url)
})

router.post('/user-login', (req, res) => {
  let { username, password } = req.body
  console.log({ username, password })
  if(!username || !password) return res.json({success: false, mssg: 'Required\s all fields'})

  findUserByUsername(username).then(user => {
    if(!user) return res.json({success: false, mssg: 'Invalid username or password'})

    comparePassword(password, user.password).then(verifyPass => {
      if(!verifyPass) return res.json({success: false, mssg: 'Invalid username or password'})

      req.session.id = user._id
      req.session.username = user.username

      res.json({success: true, mssg: `Hi ${user.username}!!`})
    }).catch(e => console.log(e))

  }).catch(ex => console.log(ex))
})

router.post('/user-signup', (req, res) => {
  let { password, username } = req.body

  if(!password || !username) return res.json({success: false, mssg: 'Required\'s fields'})

  findUserByUsername(username).then(user => {
    if(user) return res.json({success: false, mssg: 'Username already exist'})

    cryptPassword(password).then(hash => {
      let regUser = { password: hash, username, loggedIn: false }

      createUser(regUser).then(newUser => {

        res.json({success: true, mssg: `Thank you for sign up ${username}`})
      }).catch(e => console.log(e))

    }).catch(ex => console.log(ex))

  }).catch(exp => console.log(exp))
})

module.exports = router
