// MIDDLWWARES

/** FOR LOGGED IN USERS ONLY */
const LoggedIn = (req, res, next) =>
  !req.session.id ? res.redirect('/login') : next()

/** FOR NOT-LOGGED IN USERS ONLY */
const NotLoggedIn = (req, res, next) =>
  req.session.id ? res.redirect('/') : next()

module.exports = { LoggedIn, NotLoggedIn }
