const Datastore = require('nedb'),
      db = {}

db.users = new Datastore({ filename: 'path/to/users', autoload: true })

module.exports = db
