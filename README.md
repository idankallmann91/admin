# Admin Dashboard

## Prerequisites

  * nodejs.
  * npm.
  * yarn.

## Installing

##### setup

  1. Install 'yarn' globally - `npm install -g yarn`.

  2. From root directory run the command `yarn install`.

  3. Create in root directory **'.env'** file and insert you're own values:
    ```
      MODE=""
      PORT=
      SECRET_KEY=""
    ```
    * **MODE** for production insert **"production"**.
      For development leave empty **quotes**
    * **PORT** should contain **numbers** only **without quotes**
    * **SECRET_KEY** whatever you want **with quotes**

  4. Create in root directory new folder **'uploads'**, inside **uploads** create 2 new folders 1 - **'audios'** 2 - **'images'**

  5. Start the app running `yarn build`.
