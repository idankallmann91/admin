import React, { useEffect } from 'react'
import './Home.css'

// Redux Connection
import { connect } from 'react-redux'

// redux actions
import { IsLogged } from '../../store/actions/user-a'
import { UploadFiles, ToggleButton, SaveText, SetText, SetCheckbox } from '../../store/actions/board-a'
import { AddImageFiles, RemoveImageFile, AddAudioFile, RemoveAudioFile } from '../../store/actions/files-a'

// Child components
import Keyboard from '../../components/keyboard/Keyboard'
import Notify from '../../components/notify/Notify'
import Categories from '../../components/categories/Categories'

const Home = (props) => {
  const {
     keyboard,
     setCheckbox,
     checkbox,
     toggleButton } = props

  return (
    <div className="container">
      <div>
          <h1>Admin Dashboard</h1>
          <Keyboard toggleBtn={keyboard} handleToggleButton={toggleButton} />

          <Notify setCheckbox={setCheckbox} checkbox={checkbox} />

          <Categories {...props} />
      </div>
    </div>
  )
}

const mapStateToProps = state => {
  return {
    keyboard: state.boardReducer.keyboard,
    checkbox: state.boardReducer.checkbox,
    text: state.boardReducer.inputText,

    imgFiles: state.filesReducer.imageFiles,
    imgNames: state.filesReducer.imagesNames,
    audioF: state.filesReducer.audioFile
  }
}

const mapDispathToProps = dispatch => {
  return {
    toggleButton: val => dispatch(ToggleButton(val)),

    setCheckbox: val => dispatch(SetCheckbox(val)),

    setText: val => dispatch(SetText(val)),
    saveText: val => dispatch(SaveText(val)),

    addImageFiles: files => dispatch(AddImageFiles(files)),
    addAudioFile: file => dispatch(AddAudioFile(file)),
    uploadFiles: files => dispatch(UploadFiles(files)),
    removeImageFile: file => dispatch(RemoveImageFile(file)),
    removeAudioFile: file => dispatch(RemoveAudioFile(file))
  }
}

export default connect(mapStateToProps, mapDispathToProps)(Home)
