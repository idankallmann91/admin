// import { post } from 'axios'
window.onload = () => {
// User signup

  if(document.querySelector('#app')) return

  document.querySelector('form.form_register').addEventListener('submit', function(e) {
    e.preventDefault()

    let
      username = document.querySelector('.r_username').value,
      password = document.querySelector('.r_password').value

    if (!username || !password) {
      console.log('Values are missing!!')
    } else if (password != password_again) {
      console.log('Passwords don\'t match!!')
    } else {

      let signupOpt = {
        data: { username, password },
        btn: document.querySelector('.r_submit'),
        url: '/user-signup',
        redirect: '/',
        defBtnValue: 'Signup for free'
      }
      commonLogin(signupOpt)
    }
  })

  // User login
  document.querySelector('form.form_login').addEventListener('submit', function(e) {
    e.preventDefault()

    let
      username = document.querySelector('.l_username').value,
      password = document.querySelector('.l_password').value

    if (!username || !password) return console.log('Values are missing!!')

    let loginOpt = {
      data: { username, password },
      btn: document.querySelector('.l_submit'),
      url: '/user-login',
      redirect: '/',
      defBtnValue: 'Login to continue'
    }
    commonLogin(loginOpt)
  })

  function commonLogin(options) {
    let { data, btn, url, redirect, defBtnValue } = options

    const xhttp = new XMLHttpRequest()
    xhttp.onreadystatechange = function() {

        if (this.readyState == 4 && this.status == 200) {
          let res = JSON.parse(xhttp.responseText)

           if (res.success) {
             console.log(res.mssg)
             setTimeout(() => {location.href = redirect},2500)
           } else {
             console.log(res.mssg)
           }
        }
    }
    xhttp.open("POST", url, true)
    xhttp.setRequestHeader('Content-type', 'application/json')
    xhttp.send(JSON.stringify(data))
  }

}
