import { post, get } from 'axios'

export const LogOut = () => {
  return (dispatch) => {
    get('/logout').then(() => {
      dispatch({ type: 'SIGNOUT' })
    })
  }
}
