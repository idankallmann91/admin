const initState = { keyboard: false, inputText: '', checkbox: false }

const boardReducer = (state = initState, action) => {
  switch (action.type) {

    // change input text:
    case 'INPUT_TEXT':
      state = {
        ...state,
        inputText: action.text
      }
      break
      // change input text:
    case 'CHECKBOX':
      state = {
        ...state,
        checkbox: action.check
      }
      break
      // btn on/off toggle case:
    case 'ON_OFF':
      state = {
        ...state,
        keyboard: action.keyboard
      }
      break
      default:
      state = {...state}
  }

  return state
}

export default boardReducer
