const initState = { imageFiles: [], audioFile: [] }

const filesReducer = (state = initState, action) => {
  switch (action.type) {
      case 'ADD_IMAGE_FILES':
        state = {
          ...state,
          imageFiles: [...state.imageFiles, ...action.files]
        }
        break
      case 'ADD_AUDIO_FILE':
        state = {
          ...state,
          audioFile: [action.file]
        }
        break
      case 'REMOVE_IMAGE_FILES':
        state = {
          ...state,
          imageFiles: state.imageFiles.filter((el, i) => i !== action.id)
        }
        break
      case 'REMOVE_AUDIO_FILE':
        state = {
          ...state,
          audioFile: []
        }
        break
    default:
    state = {...state}
  }

  return state
}

export default filesReducer
