const initState = { userDetails: {}, loggedIn: false, mssg: '' }

const authReducer = (state = initState, action) => {
  switch (action.type) {
    // user auth cases:
    case 'LOGIN_SUCCESS':
    console.log(action);
      state = {
        ...state,
        loggedIn: action.success,
        mssg: action.mssg
      }
      break;
    case 'LOGIN_ERROR':
    console.log(action);
      state = {
        ...state,
        loggedIn: action.success,
        mssg: action.mssg
      }
      break
    case 'SIGNUP_SUCCESS':
      state = {
        ...state,
        mssg: action.mssg
      }
      break
    case 'SIGNUP_ERROR':
      state = {
        ...state,
        mssg: action.mssg
      }
      break
    case 'SIGNOUT':
      state = {
        ...state,
        userDetails: {}
      }
      break
    case 'IS_LOGGED':
      state = {
        ...state,
        loggedIn: action.user.success
      }
      break

    default:
    state = {...state}
  }

  return state
}

export default authReducer
