import React from 'react'
import { BrowserRouter as Router, Route, Switch } from "react-router-dom"

import Home from '../views/home/Home'
import Header from './header/Header'
import Footer from './footer/Footer'

import './App.css'
const App = () => {
  return (
    <Router>
        <div>
            <Header />
            <Switch>
                <Route exact path="/" component={Home} />
                <Route component={Home} />
            </Switch>
            <Footer />
        </div>
    </Router>
  )
};

export default App
