import React from 'react'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import Switch from '@material-ui/core/Switch'
import './Keyboard.css'


const Keyboard = ({ toggleBtn, handleToggleButton }) => {

  return (
    <div className="switch">
        <FormControlLabel
           control={
             <Switch
               checked={toggleBtn}
               onChange={() => handleToggleButton()}
               value="keyboard"
               color="primary"
             />
           }
         />
         <span onClick={() => handleToggleButton()}>
            <i id="keyboardIcon" className="material-icons">keyboard</i>
         </span>
    </div>
  )
}

export default Keyboard
