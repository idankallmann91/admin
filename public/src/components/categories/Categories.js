import React, { useState } from 'react'

// Children components
import Dnd from '../dnd/Dnd'
import PaperGrid from './PaperGrid'
import Audio from './Audio'

// material-ui components
import MenuItem from '@material-ui/core/MenuItem'
import FormControl from '@material-ui/core/FormControl'
import InputLabel from '@material-ui/core/InputLabel'
import TextField from '@material-ui/core/TextField'
import Select from '@material-ui/core/Select'
import Delete from '@material-ui/icons/Delete'
import CloudUploadIcon from '@material-ui/icons/CloudUpload'
import Button from '@material-ui/core/Button'
import './Categories.css'

const Categories = (props) => {
  const { imgFiles, audioF, text, uploadFiles, addImageFiles, removeImageFile, addAudioFile, saveText, setText } = props,
        [category, setCategory] = useState(''),

        handleDropParent = files => addImageFiles(files),
        handleSubmit = e => {
          e.preventDefault()
          let formData = new FormData(), i,
              len = imgFiles.length
          for (i = 0; i < len; i++) {
            formData.append('images', imgFiles[i])
          }
          formData.append('audio', audioF[0])
          uploadFiles(formData)
        }

  return (
    <div>
        <form className="" autoComplete="off" encType="multipart/form-data" onSubmit={handleSubmit}>
            <FormControl className="textField">
                <TextField
                  id="outlined-name"
                  label="Text"
                  value={text}
                  onChange={e => setText(e.target.value)}
                  margin="normal"
                  variant="outlined"
                  />
                  <Button onClick={() => saveText(text)} variant="contained" color="primary">
                    Save
                  </Button>
            </FormControl>
            <br />
            <FormControl className="selectWrap">
                <InputLabel htmlFor="age-simple">Categories</InputLabel>
                <Select
                  onChange={e => setCategory(e.target.value)}
                  value={category}
                >
                    <MenuItem value=""></MenuItem>
                    <MenuItem value="category-1">Categories 1</MenuItem>
                    <MenuItem value="category-2">Categories 2</MenuItem>
                    <MenuItem value="category-3">Categories 3</MenuItem>
                    <MenuItem value="category-4">Categories 4</MenuItem>
                </Select>
            </FormControl>
            <br/ ><br/ >
            <div>
                <div id="upload-btn-wrapper">
                    <button id="btn">Upload image files</button>
                    <input
                      type="file"
                      name="images"
                      id="uploadFile"
                      accept="image/*"
                      onChange={e => addImageFiles(e.target.files)}
                    />
                </div>
                <div className="imageWrapper">
                  {
                    imgFiles.length ? <PaperGrid images={imgFiles}/> : null
                  }
                </div>
                <Dnd {...props} handleDropParent={handleDropParent}>
                    <div id="DND">
                      {imgFiles.map((img, i) =>
                        (
                          <div key={i.toString()}>
                              <p>{img.name}</p>
                              <Delete onClick={() => removeImageFile(i)}/>
                          </div>
                        ))}
                    </div>
                </Dnd>
            </div>

            <Audio audioF={audioF} addAudioFile={addAudioFile}/>
            <Button variant="contained" type="submit" color="primary">
                Upload <CloudUploadIcon style={{marginLeft:'10px'}} />
            </Button>
        </form>


    </div>
  )
}

export default Categories
