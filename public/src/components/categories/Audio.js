import React from 'react'

const Audio = ({ audioF, addAudioFile }) => {

  return (
    <div>
        <div id="upload-btn-wrapper">
            <button id="btn">Upload mp3 file</button>
            <input
              type="file"
              name="audio"
              id="uploadFile"
              accept="audio/*"
              onChange={e => addAudioFile(e.target.files[0])}
            />
        </div>
        <div>
          { (audioF.length > 0) ?
            <audio id="sound" src={URL.createObjectURL(audioF[0])} controls></audio> : null
          }
        </div>
    </div>
  );
}

export default Audio
