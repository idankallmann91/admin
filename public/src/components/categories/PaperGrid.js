import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import GridList from '@material-ui/core/GridList'
import GridListTile from '@material-ui/core/GridListTile'

const styles = theme => ({
  root: {
    margin:'20px 0',
    display: 'flex',
    flexWrap: 'wrap',
    overflow: 'hidden',
    backgroundColor: theme.palette.background.paper
  },
  gridList: {
    width: 500,
    height: 450
  },
  gridListSmall: {
    width: 500,
    height: 'auto'
  },
});

const PaperGrid = (props) => {
  const { classes, images } = props,

  addPaper = images.map((img, i) => {
    return (
            <GridListTile key={i} cols={i % 4 === 0 ? 3 : 1}>
              <img className="image" key={i} alt={img.name} src={URL.createObjectURL(img)} />
            </GridListTile>
          )
  })

  return (
    <div className={classes.root}>
      <GridList
        cellHeight={160}
        className={images.length >= 5 ? classes.gridList : classes.gridListSmall}
        cols={3}>
          {addPaper}
      </GridList>
    </div>
  );
}

PaperGrid.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(PaperGrid)
