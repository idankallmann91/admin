import React, { useState } from 'react'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import Checkbox from '@material-ui/core/Checkbox'
import './Notify.css'


const Notify = ( {checkbox, setCheckbox} ) => {
  return (
    <div>
        <FormControlLabel
          control={
            <Checkbox
                checked={checkbox}
                onChange={() => setCheckbox(!checkbox)}
                color="primary"
            />
          }
          label=""
        />
    </div>
  )
}

export default Notify
