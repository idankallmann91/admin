import React, { useState, useEffect, useRef } from 'react'
import './Dnd.css'

const Dnd = (props) => {

  const { handleDropParent, children, imgFiles } = props,

        dropRef = useRef(null),
        [dragging, setDragging] = useState(true),
        [dragCounter, setDragCounter] = useState(0),

        handleDrag = e => {
          e.preventDefault()
          e.stopPropagation()
        },
        handleDragIn = e => {
          e.preventDefault()
          e.stopPropagation()
          let i = dragCounter + 1,
              items = e.dataTransfer.items
          setDragCounter(i)
          if ( items && items.length > 0) {
            setDragging(true)
          }
        },
        handleDragOut = e => {
          e.preventDefault()
          e.stopPropagation()
          let i = dragCounter - 1
          setDragCounter(i)
          if(dragCounter !== 0) return
          setDragging(false)
        },
        handleDrop = e => {
          e.preventDefault()
          e.stopPropagation()
          let files = e.dataTransfer.files
          if(files.length >= 25 ) return console.log("Maximux 24 image's uploads");
          if (files && files.length > 0) {
            handleDropParent(files)
            e.dataTransfer.clearData()
            setDragCounter(0)
          }
        }

  useEffect(() => {
    let div = dropRef.current

    div.addEventListener('dragenter', handleDragIn)
    div.addEventListener('dragleave', handleDragOut)
    div.addEventListener('dragover', handleDrag)
    div.addEventListener('drop', handleDrop)

    return () => {
      div.removeEventListener('dragenter', handleDragIn)
      div.removeEventListener('dragleave', handleDragOut)
      div.removeEventListener('dragover', handleDrag)
      div.removeEventListener('drop', handleDrop)
    }
  },[])

  return (
    <div>
        <div id="dragCover" ref={dropRef}>
            { (!dragging || !imgFiles.length) &&
              <div id="dashBoard">
                  <div>
                    <div>drop here :)</div>
                  </div>
              </div>
            }
            {children}
        </div>
    </div>
  )
}

export default Dnd
