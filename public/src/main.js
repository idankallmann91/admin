/* For not logged in users */
import './user-system/user-system'

import React from 'react'
import ReactDOM from 'react-dom'
import App from './components/App'

import { createStore, applyMiddleware } from 'redux'
import { Provider } from 'react-redux'
import thunk from 'redux-thunk'
import rootReducer from './store/rootReducer'

const store = createStore(rootReducer, applyMiddleware(thunk))

if(document.querySelector('#app')) {
  ReactDOM.render(
    <Provider store={store}><App /></Provider>,
    document.getElementById('app')
  )
}
